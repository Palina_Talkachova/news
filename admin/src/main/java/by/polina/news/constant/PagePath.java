package by.polina.news.constant;

/**
 * Created by jimik on 26.11.2016.
 */
public class PagePath {
    public static final String HOME_PAGE = "/index.jsp";
    public static final String ERROR_PAGE = "/error.jsp";
    public static final String NEWS_PAGE = "/news.jsp";
}
