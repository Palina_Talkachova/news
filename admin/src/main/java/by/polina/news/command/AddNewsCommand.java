package by.polina.news.command;

import by.polina.news.constant.PagePath;


import by.polina.news.entity.News;
import by.polina.news.service.impl.NewsServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by jimik on 26.11.2016.
 */
public class AddNewsCommand implements ActionCommand {

    private static final Logger LOGGER = Logger
            .getLogger(AddNewsCommand.class);


    /**
     * takes a list with books and sets as an attribute
     *
     * @param request
     * @return page
     */
    @Override
    public String execute(HttpServletRequest request) {

        String page;
        News news = null;

        NewsServiceImpl newsService= new NewsServiceImpl();
        try {
          /*  news.setCreationDay(new Date());
            news.setFullText("ddd");
            news.setShortText("f");
            news.setModificationDay(new Date());
            news.setTitle("rr");

            newsService.add(news);*/


            request.setAttribute("news", news);
            request.getSession().setAttribute("dd","HelloWorld");
            request.getSession().setAttribute("ff", "00000");

            page = PagePath.NEWS_PAGE;

        } catch (Exception e) {
            page = PagePath.ERROR_PAGE;
            request.setAttribute("error_message",
                    "Error in TakeAllBooksCommand ");
        }
        return page;
    }
}