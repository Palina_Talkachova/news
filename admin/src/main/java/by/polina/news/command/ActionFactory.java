package by.polina.news.command;



import javax.servlet.http.HttpServletRequest;

/**
 * Created by jimik on 26.11.2016.
 */
public class ActionFactory {

    private static final String PARAM = "param";

    public ActionCommand defineCommand(HttpServletRequest request) {

        String parameter = request.getParameter(PARAM);                                         // gets a value of "param" from jsp
        ActionCommand actionCommand;

        if (parameter == null || parameter.isEmpty()) {
            actionCommand = new AddNewsCommand();
        } else {
            CommandEnum paramEnum = CommandEnum.valueOf
                    (parameter.toUpperCase());
            actionCommand = paramEnum.getCommand();                                            // defines and gets  an object of necessary Command class
        }
        return actionCommand;
    }
}

