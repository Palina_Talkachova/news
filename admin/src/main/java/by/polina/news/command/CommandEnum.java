package by.polina.news.command;

/**
 * Created by jimik on 26.11.2016.
 */
enum CommandEnum {


    ADD_NEWS {
        {
            this.command = new AddNewsCommand();
        }
    };

    ActionCommand command;

    public ActionCommand getCommand() {
        return command;
    }
}


