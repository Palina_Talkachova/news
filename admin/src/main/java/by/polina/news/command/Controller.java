package by.polina.news.command;



import by.polina.news.config.DBConnection;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by jimik on 26.11.2016.
 */
public class Controller extends HttpServlet {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(Controller.class);


    /**
     * gets and configures log4j
     *
     * @param config
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {

     /*   super.init(config);
        String log4j = config.getInitParameter("log4j_pass");                               // gets the parameter (llog4j_pass) by its name

        LOGGER.info(log4j);

        String path = getServletContext().getRealPath(log4j);                               // get a real path of log4j, getServletContext() - gets a link  for a object of ServletContext class

        LOGGER.info(path);
        PropertyConfigurator.configure(path); */                                              // configures log4j
    }


    /**
     * process a request
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }


    /**
     * process a request
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }


    /**
     * close connections in the pool
     */
    @Override
    public void destroy() {

        super.destroy();
        DBConnection.getInstance().cleanUp();
    }


    /**
     * defines a command, do necessary actions and calls a page in response
     * to the request
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
   void processRequest(HttpServletRequest request,
                                HttpServletResponse response)
            throws ServletException, IOException {

        String page;
        ActionFactory actionFactory = new ActionFactory();
        ActionCommand command = actionFactory.defineCommand(request);                           // defines a command from the request and gets an object of concrete Command class

        page = command.execute(request);                                                        // does necessary actions in defined Command class

        RequestDispatcher dispatcher = getServletContext().
                getRequestDispatcher(page);
        dispatcher.forward(request, response);                                                  // calls the concrete page in response to the request
    }
}



