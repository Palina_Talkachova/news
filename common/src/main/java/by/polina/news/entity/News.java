package by.polina.news.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * Created by jimik on 19.11.2016.
 */
public class News implements  Serializable{


    Long newsId;
    String title;
    String shortText;
    String fullText;
    Date creationDay;
    Date modificationDay;

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Date getCreationDay() {
        return creationDay;
    }

    public void setCreationDay(Date creationDay) {
        this.creationDay = creationDay;
    }

    public Date getModificationDay() {
        return modificationDay;
    }

    public void setModificationDay(Date modificationDay) {
        this.modificationDay = modificationDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (!creationDay.equals(news.creationDay)) return false;
        if (!fullText.equals(news.fullText)) return false;
        if (!modificationDay.equals(news.modificationDay)) return false;
        if (!newsId.equals(news.newsId)) return false;
        if (!shortText.equals(news.shortText)) return false;
        if (!title.equals(news.title)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = newsId.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + shortText.hashCode();
        result = 31 * result + fullText.hashCode();
        result = 31 * result + creationDay.hashCode();
        result = 31 * result + modificationDay.hashCode();
        return result;
    }
}
