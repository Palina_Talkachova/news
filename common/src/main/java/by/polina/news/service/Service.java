package by.polina.news.service;

/**
 * Created by jimik on 22.11.2016.
 */
public interface Service<T> {

    void add (T t);
    T get (Integer id);
    void update (T t);
    void delete (Integer id);
}
