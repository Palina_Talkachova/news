package by.polina.news.service;

import by.polina.news.entity.News;

/**
 * Created by jimik on 22.11.2016.
 */
public interface NewsService extends Service<News>{

    @Override
    void add(News news);


}
