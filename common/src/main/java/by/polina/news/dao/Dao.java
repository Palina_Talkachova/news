package by.polina.news.dao;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created by jimik on 13.11.2016.
 */
public interface Dao<T> {

    void add (T t);
    T get (Integer id);
    void update (T t);
    void delete (Integer id);
    List<T> getAll();

    default void closeStatement(Statement statement) {                                      // it isn't necessary to implement it
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {

            }
        }
    }
}