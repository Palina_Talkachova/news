package by.polina.news.dao;

import by.polina.news.entity.Author;

/**
 * Created by jimik on 13.11.2016.
 */
public interface AuthorDao extends Dao<Author> {


}
