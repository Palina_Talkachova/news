package by.polina.news.dao;

import by.polina.news.entity.Tag;

/**
 * Created by jimik on 13.11.2016.
 */
public interface TagDao extends Dao<Tag> {

    @Override
    void add(Tag tag);

    @Override
    Tag get(Integer id);

    @Override
    void update(Tag tag);

    @Override
    void delete(Integer id);
}
