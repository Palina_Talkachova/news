package by.polina.news.dao;

import by.polina.news.entity.Comment;

/**
 * Created by jimik on 13.11.2016.
 */
public interface CommentDao extends Dao<Comment> {

    @Override
    void add(Comment comment);

    @Override
    Comment get(Integer id);

    @Override
    void update(Comment comment);

    @Override
    void delete(Integer id);
}
