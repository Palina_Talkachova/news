package by.polina.news.dao;

import by.polina.news.entity.User;

/**
 * Created by jimik on 13.11.2016.
 */
public interface UserDao extends Dao<User> {

    @Override
    void add(User user);

    @Override
    User get(Integer id);

    @Override
    void update(User user);

    @Override
    void delete(Integer id);
}
