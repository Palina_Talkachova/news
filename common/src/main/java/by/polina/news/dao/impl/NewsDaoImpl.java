package by.polina.news.dao.impl;

import by.polina.news.config.DBConnection;
import by.polina.news.config.DBConnection3;
import by.polina.news.dao.NewsDao;
import by.polina.news.entity.News;

import java.sql.*;
import java.sql.Statement;
import java.util.List;

/**
 * Created by jimik on 13.11.2016.
 */
public class NewsDaoImpl implements NewsDao {

    private static final String ADD_NEWS_SQL = "INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String SELECT_LAST_NEWS_ID_SQL = "SELECT max(news_id) from news";
    private static final String MAX_NEWS_ID_SQL = "max(news_id)";

    @Override
    public void add(News news) {
        Connection connection;
        PreparedStatement preparedStatement;
        connection = DBConnection.getInstance().takeConnection();
        try {
            preparedStatement = connection.prepareStatement(ADD_NEWS_SQL);
            Long newsId = getLastNewsId();
            news.setNewsId(newsId);
            preparedStatement.setLong(1, newsId);
            preparedStatement.setString(2, news.getTitle());
            preparedStatement.setString(3, news.getShortText());
            preparedStatement.setString(4, news.getFullText());
            preparedStatement.setDate(5, (Date) news.getCreationDay());
            preparedStatement.setDate(6, (Date) news.getModificationDay());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public News get(Integer id) {
        return null;
    }

    @Override
    public void update(News news) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public List<News> getAll() {
        return null;
    }

    private Long getLastNewsId() {
        Long lastNewsId = null;
        Connection connection = DBConnection.getInstance().takeConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_LAST_NEWS_ID_SQL);
        while (resultSet.next()) {
            lastNewsId =resultSet.getLong(MAX_NEWS_ID_SQL);
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeStatement(statement);
            DBConnection.getInstance().returnConnection(connection);

        }
        return  lastNewsId;
    }
}
