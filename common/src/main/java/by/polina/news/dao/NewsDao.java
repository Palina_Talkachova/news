package by.polina.news.dao;

import by.polina.news.entity.News;

/**
 * Created by jimik on 13.11.2016.
 */
public interface NewsDao extends Dao<News> {

    @Override
    void add(News news);

    @Override
    News get(Integer id);

    @Override
    void update(News news);

    @Override
    void delete(Integer id);
}
